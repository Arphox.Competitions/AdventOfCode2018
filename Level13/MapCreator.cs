﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Level13
{
    static class MapCreator
    {
        public static char[,] CreateMapView(string[] inputLines)
        {
            int longestLineLength = inputLines.Max(x => x.Length);
            char[,] charMap = new char[longestLineLength, inputLines.Length]; // xLength, yLength

            FillWithSpaces(charMap);
            FillMapWithContent(inputLines, charMap);

            return charMap;
        }

        public static List<Cart> CreateCarts(char[,] mapView)
        {
            List<Cart> carts = new List<Cart>();

            for (int y = 0; y < mapView.GetLength(1); y++)
            {
                for (int x = 0; x < mapView.GetLength(0); x++)
                {
                    char currentChar = mapView[x, y];
                    if (currentChar.IsCartChar())
                    {
                        Point position = new Point(x, y);
                        Cart cart = new Cart(position, mapView);
                        carts.Add(cart);
                    }
                }
            }

            return carts;
        }

        private static void FillWithSpaces(char[,] charMap)
        {
            for (int i = 0; i < charMap.GetLength(0); i++)
                for (int j = 0; j < charMap.GetLength(1); j++)
                    charMap[i, j] = ' ';
        }

        private static void FillMapWithContent(string[] inputLines, char[,] charMap)
        {
            for (int y = 0; y < inputLines.Length; y++)
            {
                string line = inputLines[y];
                for (int x = 0; x < line.Length; x++)
                {
                    charMap[x, y] = line[x];
                }
            }
        }

        private static void Print(char[,] charMap)
        {
            for (int y = 0; y < charMap.GetLength(1); y++)
            {
                for (int x = 0; x < charMap.GetLength(0); x++)
                {
                    Console.Write(charMap[x, y]);
                }

                Console.WriteLine();
            }
        }
    }
}
﻿namespace Level13
{
    internal enum TurnStrategy { Left, Straight, Right }
    internal enum Facing { Left, Right, Up, Down }
}
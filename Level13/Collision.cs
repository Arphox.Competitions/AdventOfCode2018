﻿using System.Collections.Generic;

namespace Level13
{
    internal sealed class Collision
    {
        internal Point Position { get; }
        internal List<Cart> AffectedCarts { get; }

        internal Collision(Point position, List<Cart> affectedCarts)
        {
            Position = position;
            AffectedCarts = affectedCarts;
        }
    }
}
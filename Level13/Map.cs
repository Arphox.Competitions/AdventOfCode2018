﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Level13
{
    internal sealed class Map
    {
        internal List<Cart> Carts { get; private set; }

        private char[,] mapView;

        internal Map(string[] inputLines)
        {
            mapView = MapCreator.CreateMapView(inputLines);
            Carts = MapCreator.CreateCarts(mapView);
        }

        internal CollisionInfo MoveCarts()
        {
            List<Collision> collisions = new List<Collision>();

            for (int i = Carts.Count - 1; i >= 0; i--)
            {
                Collision collision = MoveCart(Carts[i]);
                if (collision != null)
                    collisions.Add(collision);
            }

            RemoveCollidedCarts(collisions);
            return new CollisionInfo(collisions);
        }

        internal Collision MoveCart(Cart cart)
        {
            cart.Move();

            // DETECT collisions
            return null; // TODO
        }

        private bool IsAtIntersection(Cart cart)
        {
            int x = cart.Position.X;
            int y = cart.Position.Y;
            return mapView[x, y] == '+';
        }

        private bool IsAtCorner(Cart cart)
        {
            int x = cart.Position.X;
            int y = cart.Position.Y;
            return mapView[x, y] == '/' || mapView[x, y] == '\\';
        }

        private void RemoveCollidedCarts(List<Collision> collisions)
        {
            var allAffectedCarts = collisions.SelectMany(c => c.AffectedCarts).Distinct();

            foreach (Cart affectedCart in allAffectedCarts)
                Carts.Remove(affectedCart);
        }
    }
}
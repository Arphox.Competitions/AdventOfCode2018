﻿using System;

namespace Level13
{
    internal sealed class Point : IEquatable<Point>
    {
        internal int X { get; set; }
        internal int Y { get; set; }

        internal Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override string ToString() => $"{X},{Y}";
        public override int GetHashCode() => X ^ Y;
        public override bool Equals(object obj) => obj is Point p ? Equals(p) : false;
        public bool Equals(Point other) => other.X == X && other.Y == Y;
        public static bool operator ==(Point a, Point b) => Equals(a, b);
        public static bool operator !=(Point a, Point b) => !(a == b);
    }
}

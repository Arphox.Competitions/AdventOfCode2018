﻿using System;
using System.Linq;

namespace Level13
{
    internal sealed class Cart
    {
        internal char TileChar => Helpers.FacingToChar(facing);
        internal Point Position { get; }

        private Facing facing;
        private TurnStrategy turnStrategy;
        private char[,] mapView;
        private char MapTileUnderCart => mapView[Position.X, Position.Y];

        internal Cart(Point position, char[,] mapView)
        {
            Position = position ?? throw new ArgumentNullException(nameof(position));
            this.mapView = mapView ?? throw new ArgumentNullException(nameof(mapView));
            facing = Helpers.CharToFacing(mapView.ItemAt(Position));
        }

        internal void Move()
        {
            Position.Move(facing);
            
            TurnAtIntersectionIfNeeded();
            TurnAtCornerIfNeeded();
        }

        private void TurnAtIntersectionIfNeeded()
        {
            if (MapTileUnderCart == '+')
            {
                facing = Helpers.GetNextFacing(facing, turnStrategy);
                turnStrategy = Helpers.GetNextTurnStrategy(turnStrategy);
            }
        }

        private void TurnAtCornerIfNeeded()
        {
            facing = Helpers.TurnAtCornerIfNeeded(facing, turnStrategy, MapTileUnderCart);
        }

        public override string ToString() => $"{TileChar} at {Position}";
    }
}
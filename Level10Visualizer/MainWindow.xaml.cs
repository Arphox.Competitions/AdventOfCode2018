﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;
using AdventOfCode;
using MyPoint = AdventOfCode.Point;

namespace AoCWpf
{
    public partial class MainWindow : Window
    {
        private const string realPath = Program.PathBase + "level10.txt";
        private const string demoPath = Program.PathBase + "level10demo.txt";
        private static readonly List<MyPoint> points = new List<MyPoint>();
        private static readonly List<MyPoint> velocities = new List<MyPoint>();
        private static int counter = 0;
        private DispatcherTimer timer = new DispatcherTimer();

        public MainWindow()
        {
            InitializeComponent();
            timer.Interval = TimeSpan.FromMilliseconds(20);
            timer.Tick += Timer_Tick;
            Run();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            const int solution = 10136;
            const int diffcap = 5;
            int diff = Math.Abs(solution - counter);
            if (diff == 0)
            {
                timer.Interval = TimeSpan.FromMilliseconds(1000);
            }
            else if (diff < diffcap)
            {
                int wait = (diffcap - diff) * 50;
                timer.Interval = TimeSpan.FromMilliseconds(15 + wait);
            }
            else
            {
                timer.Interval = TimeSpan.FromMilliseconds(15);
            }
            PerformStep();

            if (counter > 10200)
                timer.Stop();
        }

        public void Run()
        {
            string[] lines = File.ReadAllLines(realPath);

            foreach (string line in lines)
            {
                string[] parts = line.Split(new char[] { '=', '<', ' ', ',', '>' }, StringSplitOptions.RemoveEmptyEntries);
                int posX = int.Parse(parts[1]);
                int posY = int.Parse(parts[2]);
                points.Add(new MyPoint(posX, posY));

                int velX = int.Parse(parts[4]);
                int velY = int.Parse(parts[5]);
                velocities.Add(new MyPoint(velX, velY));
            }

            //double minX = points.Min(p => p.X);
            //double minY = points.Min(p => p.Y);
            //// every point to the the positive values
            //foreach (MyPoint p in points)
            //{
            //    p.X -= minX;
            //    p.Y -= minY;
            //}

            //double previous = int.MaxValue;
            //double current = points.Sum(x => Math.Abs(x.X) + Math.Abs(x.Y));
            //int counter = 0;
            //while (current < previous)
            //{
            //    counter++;
            //    ApplyVelocities();
            //    previous = current;
            //    current = points.Sum(x => Math.Abs(x.X) + Math.Abs(x.Y));
            //}
            //Console.WriteLine(counter);

            for (int i = 0; i < 10050; i++)
            {
                ApplyVelocities();
                counter++;
            }

            double minX = points.Min(p => p.X);
            double minY = points.Min(p => p.Y);
            // every point to the the positive values
            foreach (MyPoint p in points)
            {
                p.X -= minX;
                p.Y -= minY;
            }
        }

        private static void ApplyVelocities()
        {
            for (int i = 0; i < velocities.Count; i++)
            {
                points[i].X += velocities[i].X;
                points[i].Y += velocities[i].Y;
            }
        }

        private void ScalePoints()
        {
            double maxX = points.Max(p => p.X);
            double maxY = points.Max(p => p.Y);
            double xScale = myCanvas.Width / maxX;
            double yScale = myCanvas.Height / maxY;

            for (var i = 0; i < points.Count; i++)
            {
                MyPoint p = points[i];
                p.X = p.X * xScale;
                p.Y = p.Y * yScale;

                //MyPoint v = velocities[i];
                //v.X = v.X * xScale;
                //v.Y = v.Y * yScale;
            }
        }

        private void Display()
        {
            const double multiplier = 0.3;
            const int dotSize = 1;

            foreach (MyPoint p in points)
            {
                Ellipse currentDot = new Ellipse();
                currentDot.Stroke = new SolidColorBrush(Colors.Black);
                currentDot.StrokeThickness = 3;
                Canvas.SetZIndex(currentDot, 3);
                currentDot.Height = dotSize;
                currentDot.Width = dotSize;
                currentDot.Fill = new SolidColorBrush(Colors.Black);
                currentDot.Margin = new Thickness(p.X * multiplier, p.Y * multiplier, 0, 0);
                myCanvas.Children.Add(currentDot);
            }

            counter++;
            Console.WriteLine(counter);
        }

        private void MainWindow_OnKeyDown(object sender, KeyEventArgs e)
        {
            timer.Start();
        }

        private void PerformStep()
        {
            myCanvas.Children.Clear();

            Display();

            for (int i = 0; i < velocities.Count; i++)
            {
                points[i].X += velocities[i].X;
                points[i].Y += velocities[i].Y;
            }

            for (int i = 0; i < velocities.Count; i++)
            {
                points[i].X -= 4.8;
                points[i].Y -= 4.8;
            }
        }

        const double ScaleRate = 1.1;
        private void Canvas_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (e.Delta > 0)
            {
                st.ScaleX *= ScaleRate;
                st.ScaleY *= ScaleRate;
            }
            else
            {
                st.ScaleX /= ScaleRate;
                st.ScaleY /= ScaleRate;
            }

            Console.WriteLine(st.ScaleX);
            Console.WriteLine(st.ScaleY);
        }
    }
}

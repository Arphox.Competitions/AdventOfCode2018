﻿using System;

namespace Common.Extensions
{
    public static class StringExtensions
    {
        public static string[] SplitByCRLF(this string input)
        {
            return input.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}
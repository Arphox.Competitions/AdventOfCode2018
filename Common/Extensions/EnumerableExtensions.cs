﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.Extensions
{
    public static class EnumerableExtensions
    {
        public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            foreach (var item in enumerable)
            {
                action(item);
            }
        }

        public static IEnumerable<T> TakeLast<T>(this IEnumerable<T> source, int n)
        {
            return source.Skip(Math.Max(0, source.Count() - n));
        }

        public static IEnumerable<T> TakeLast<T>(this List<T> source, int n)
        {
            int sourceLength = source.Count;
            n = Math.Min(sourceLength, n);

            T[] lastNelements = new T[n];
            int i = 0;
            while (i < n)
            {
                lastNelements[n - i - 1] = source[sourceLength - i - 1];
                i++;
            }

            return lastNelements;
        }
    }
}
﻿using System.IO;

namespace Common
{
    public static class InputProvider
    {
        private const string PathBase = @"..\..\..\data\";

        public static string GetInputFor(int levelNumber, bool isDemo = false)
        {
            string optionalDemoPart = isDemo ? "demo" : "";

            string path = Path.Combine(PathBase, $"level{levelNumber}{optionalDemoPart}.txt");

            return File.ReadAllText(path);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Level13
{
    public static partial class Level13
    {
        private enum TurnStrategy { Left, Straight, Right }

        private sealed class Map
        {
            private char[,] roadMap;
            private readonly List<Cart> carts = new List<Cart>();

            internal int RemainingCartCount => carts.Count;
            internal bool OnlyOneCartLeft => carts.Count == 1;
            internal bool IsThereAnyCollision { get; private set; }
            internal L13Point CollisionPoint { get; private set; }

            internal Map(string[] rawMap, List<Cart> carts)
            {
                if (rawMap == null) throw new ArgumentNullException(nameof(rawMap));
                this.carts = carts ?? throw new ArgumentNullException(nameof(carts));

                int longestLineLength = rawMap.Max(line => line.Length);
                roadMap = new char[longestLineLength, rawMap.Length];
                FillMapWithSpaces();
                FillMapContent();

                // ----------------- LOCAL METHODS -----------------
                void FillMapWithSpaces()
                {
                    for (int i = 0; i < roadMap.GetLength(0); i++)
                        for (int j = 0; j < roadMap.GetLength(1); j++)
                            roadMap[i, j] = ' ';
                }
                void FillMapContent()
                {
                    for (int y = 0; y < rawMap.Length; y++)
                    {
                        for (int x = 0; x < rawMap[y].Length; x++)
                        {
                            roadMap[x, y] = rawMap[y][x];
                        }
                    }
                }
            }

            internal L13Point GetLastCartPosition()
            {
                if (!OnlyOneCartLeft)
                    throw new InvalidOperationException();

                return carts.Single().Coordinate;
            }

            internal void Print()
            {
                if (!PRINT_MODE_ON)
                    return;

                // Print map
                Console.ForegroundColor = ConsoleColor.DarkGray;
                for (int y = 0; y < roadMap.GetLength(1); y++)
                {
                    for (int x = 0; x < roadMap.GetLength(0); x++)
                        Console.Write(roadMap[x, y]);

                    Console.WriteLine();
                }

                // Print carts
                Console.ForegroundColor = ConsoleColor.Yellow;
                foreach (Cart cart in carts)
                {
                    Console.SetCursorPosition(cart.Coordinate.X, cart.Coordinate.Y);

                    if (carts.Any(c => c.Coordinate == cart.Coordinate && c != cart))
                    {
                        Console.Write('X');
                        //Console.ReadLine(); // you will have to press enter for all colliding carts, sorry
                    }
                    else
                    {
                        Console.Write(cart.TileChar);
                    }
                }

                Console.ResetColor();
            }

            internal void MoveCarts()
            {
                IsThereAnyCollision = false;
                CollisionPoint = null;

                for (int i = carts.Count - 1; i >= 0; i--)
                {
                    while (i > carts.Count - 1)
                        i--;

                    if (i < 0)
                        break;

                    MoveCart(carts[i]);
                }
            }

            private void MoveCart(Cart cart)
            {
                cart.MoveForward();

                if (IsAtIntersection(cart))
                {
                    cart.TurnAtIntersection();
                }
                else if (HasToTurn(cart))
                {
                    TurnAtCorner(cart);
                }

                if (IsColliding(cart))
                {
                    IsThereAnyCollision = true;
                    CollisionPoint = cart.Coordinate;
                    RemoveAllCollidingCarts();
                }
            }

            private bool IsColliding(Cart cart)
            {
                return carts.Any(c => c.Coordinate == cart.Coordinate && c != cart);
            }

            private void RemoveAllCollidingCarts()
            {
                for (int i = carts.Count - 1; i >= 0; i--)
                {
                    Cart cart = carts[i];
                    if (IsColliding(cart))
                    {
                        List<Cart> collidingWith = carts.Where(c => c.Coordinate == cart.Coordinate).ToList();
                        int collidingCount = collidingWith.Count;
                        collidingWith.ForEach(x => carts.Remove(x));

                        i = carts.Count; // start looking from the beginning of the list
                    }
                }
            }

            private void TurnAtCorner(Cart cart)
            {
                char wallChar = roadMap[cart.Coordinate.X, cart.Coordinate.Y];

                switch (wallChar)
                {
                    case '\\':
                        {
                            switch (cart.Facing)
                            {
                                case Facing.Right:
                                case Facing.Left: cart.TurnRight(); break;
                                case Facing.Down:
                                case Facing.Up: cart.TurnLeft(); break;
                            }
                            break;
                        }
                    case '/':
                        {
                            switch (cart.Facing)
                            {
                                case Facing.Right:
                                case Facing.Left: cart.TurnLeft(); break;
                                case Facing.Down:
                                case Facing.Up: cart.TurnRight(); break;
                            }
                            break;
                        }
                    default: throw new ApplicationException();
                }
            }

            private bool IsAtIntersection(Cart cart)
            {
                int x = cart.Coordinate.X;
                int y = cart.Coordinate.Y;
                return roadMap[x, y] == '+';
            }

            private bool HasToTurn(Cart cart)
            {
                int x = cart.Coordinate.X;
                int y = cart.Coordinate.Y;
                return roadMap[x, y] == '/' || roadMap[x, y] == '\\';
            }
        }
    }
}
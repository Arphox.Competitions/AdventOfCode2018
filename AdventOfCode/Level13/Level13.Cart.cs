﻿using System;

namespace AdventOfCode.Level13
{
    public static partial class Level13
    {
        private sealed class Cart
        {
            internal char TileChar => FacingHelper.FacingToChar(Facing);

            internal L13Point Coordinate { get; set; }

            internal Facing Facing { get; set; }

            private TurnStrategy turnStrategy;

            public Cart(L13Point coordinate, Facing facing)
            {
                Coordinate = coordinate;
                Facing = facing;
            }

            internal void TurnAtIntersection()
            {
                Facing = FacingHelper.GetNextFacing(Facing, turnStrategy);

                // Calculate next turn strategy (based on game rules)
                switch (turnStrategy)
                {
                    case TurnStrategy.Left: turnStrategy = TurnStrategy.Straight; break;
                    case TurnStrategy.Straight: turnStrategy = TurnStrategy.Right; break;
                    case TurnStrategy.Right: turnStrategy = TurnStrategy.Left; break;
                }
            }

            internal void TurnLeft() => Facing = FacingHelper.TurnLeft(Facing);
            internal void TurnRight() => Facing = FacingHelper.TurnRight(Facing);

            internal void MoveForward()
            {
                switch (Facing)
                {
                    case Facing.Left: Coordinate.X--; break;
                    case Facing.Right: Coordinate.X++; break;
                    case Facing.Up: Coordinate.Y--; break;
                    case Facing.Down: Coordinate.Y++; break;
                    default: throw new ApplicationException();
                }
            }

            public override string ToString() => $"{TileChar} at {Coordinate}";

            internal static bool IsCartChar(char ch)
            {
                return
                    ch == 'v' ||
                    ch == '^' ||
                    ch == '>' ||
                    ch == '<';
            }
        }
    }
}
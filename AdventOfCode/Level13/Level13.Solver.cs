﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

// Part 2, incorrect:
// 73,95
// 74,95
// 73,96
// 51,90
// 71,128

namespace AdventOfCode.Level13
{
    public static partial class Level13
    {
        private const bool PRINT_MODE_ON = false;

        private sealed class Solver
        {
            private Map map;

            public Solver()
            {
                const string realPath = Program.PathBase + "level13.txt";
                const string demoPath = Program.PathBase + "level13demo.txt";
                string[] map = File.ReadAllLines(realPath);

                List<Cart> carts = new List<Cart>();
                AddRealCarts();
                //AddDemoCarts();
                //AddCart(5, 5, Facing.Left);
                carts.Reverse();

                this.map = new Map(map, carts);

                // --------------------------------------------------
                void AddDemoCarts()
                {
                    AddCart(2, 0, Facing.Right);
                    AddCart(9, 3, Facing.Down);
                }
                void AddRealCarts()
                {
                    AddCart(126, 0, Facing.Left);
                    AddCart(58, 3, Facing.Right);
                    AddCart(102, 26, Facing.Right);
                    AddCart(143, 39, Facing.Up);
                    AddCart(6, 55, Facing.Up);
                    AddCart(101, 65, Facing.Down);
                    AddCart(124, 66, Facing.Down);
                    AddCart(70, 83, Facing.Down);
                    AddCart(4, 89, Facing.Down);
                    AddCart(61, 113, Facing.Right);
                    AddCart(58, 117, Facing.Right);
                    AddCart(126, 117, Facing.Up);
                    AddCart(137, 131, Facing.Left);
                    AddCart(81, 142, Facing.Right);
                    AddCart(96, 142, Facing.Right);
                    AddCart(131, 142, Facing.Left);
                    AddCart(46, 145, Facing.Left);
                }
                void AddCart(int x, int y, Facing facing)
                {
                    carts.Add(new Cart(new L13Point(x, y), facing));
                }
            }

            internal void Run()
            {
                Console.CursorVisible = false;

                map.Print();
                //Console.ReadLine();
                //Console.WriteLine("Started.");

                Console.CursorTop++;

                while (!map.OnlyOneCartLeft)
                {
                    if (PRINT_MODE_ON)
                    {
                        Console.CursorTop = 0;
                        Console.CursorLeft = 0;
                    }

                    map.MoveCarts();
                    map.Print();

                    if (map.IsThereAnyCollision)
                    {
                        PrintCrashPosition();
                        //Console.ReadLine();
                    }

                    if (PRINT_MODE_ON)
                    {
                        Thread.Sleep(80);
                    }
                }

                Console.CursorTop = 0;
                Console.CursorLeft = 0;
                PrintFinalPosition();
            }

            private void PrintCrashPosition()
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Collision at {map.CollisionPoint}, {map.RemainingCartCount} cart(s) left.");
            }

            private void PrintFinalPosition()
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"Last cart's position is {map.GetLastCartPosition()}");
            }
        }
    }
}
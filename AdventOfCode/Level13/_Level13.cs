﻿namespace AdventOfCode.Level13
{
    public static partial class Level13
    {
        public static void Run()
        {
            new Solver().Run();
        }
    }
}
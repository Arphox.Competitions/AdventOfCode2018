﻿using System;

namespace AdventOfCode.Level13
{
    public static partial class Level13
    {
        private enum Facing { Left, Right, Up, Down }

        private static class FacingHelper
        {
            internal static char FacingToChar(Facing facing)
            {
                switch (facing)
                {
                    case Facing.Left: return '<';
                    case Facing.Right: return '>';
                    case Facing.Up: return '^';
                    case Facing.Down: return 'v';
                    default: throw new ApplicationException();
                }
            }

            internal static Facing CharToFacing(char ch)
            {
                switch (ch)
                {
                    case '<': return Facing.Left;
                    case '>': return Facing.Right;
                    case '^': return Facing.Up;
                    case 'v': return Facing.Down;
                    default: throw new ApplicationException();
                }
            }

            internal static Facing GetNextFacing(Facing current, TurnStrategy turnStrategy)
            {
                switch (turnStrategy)
                {
                    case TurnStrategy.Straight: return current;
                    case TurnStrategy.Left: return TurnLeft(current);
                    case TurnStrategy.Right: return TurnRight(current);
                    default: throw new ApplicationException();
                }
            }

            internal static Facing TurnLeft(Facing facing)
            {
                switch (facing)
                {
                    case Facing.Up: return Facing.Left;
                    case Facing.Left: return Facing.Down;
                    case Facing.Down: return Facing.Right;
                    case Facing.Right: return Facing.Up;
                    default: throw new ApplicationException();
                }
            }

            internal static Facing TurnRight(Facing facing)
            {
                switch (facing)
                {
                    case Facing.Up: return Facing.Right;
                    case Facing.Right: return Facing.Down;
                    case Facing.Down: return Facing.Left;
                    case Facing.Left: return Facing.Up;
                    default: throw new ApplicationException();
                }
            }
        }
    }
}
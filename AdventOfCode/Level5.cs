﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode
{
    public static class Level5
    {
        public static void Run()
        {
            const string inputPath = Program.PathBase + "level5.txt";

            List<string> allPolys = CreateAllPolys();
            List<string> alphabet = CreateSinglePolys();

            string fileContent = File.ReadAllText(inputPath);

            Part1(allPolys, fileContent);

            Stopwatch stopper = Stopwatch.StartNew();
            Part2(allPolys, alphabet, fileContent);
            Console.WriteLine(stopper.Elapsed);

            stopper = Stopwatch.StartNew();
            Part2MultiThreaded(allPolys, alphabet, fileContent);
            Console.WriteLine(stopper.Elapsed);

            Console.WriteLine("\nDone");
        }

        private static List<string> CreateAllPolys()
        {
            List<string> allPolys = new List<string>(); //aA, Aa, bB, Bb, cC, Cc ...
            for (char c = 'a'; c <= 'z'; c++)
            {
                allPolys.Add(c + c.ToString().ToUpper()); // aA
                allPolys.Add(c.ToString().ToUpper() + c); // Aa
            }

            return allPolys;
        }

        private static List<string> CreateSinglePolys()
        {
            List<string> singlePolys = new List<string>(); // a, b, c, ...
            for (char c = 'a'; c <= 'z'; c++)
            {
                singlePolys.Add(c.ToString());
            }

            return singlePolys;
        }

        private static void Part1(List<string> allPolys, string fileContent)
        {
            StringBuilder sb = new StringBuilder(fileContent);

            while (true)
            {
                int prevLength = sb.Length;
                foreach (var v in allPolys)
                    sb.Replace(v, "");

                if (sb.Length == prevLength)
                    break;
            }

            //Console.WriteLine(sb.ToString());
            Console.WriteLine("PART 1");
            Console.WriteLine(sb.Length + "\n");
        }

        private static void Part2(List<string> allPolys, List<string> singlePolys, string fileContent)
        {
            Console.WriteLine("PART 2");

            int min = int.MaxValue;
            foreach (string pair in singlePolys)
            {
                Console.Write(pair);
                string lowerCase = pair;
                string upperCase = pair.ToUpper();

                StringBuilder sb = new StringBuilder(fileContent);

                while (true)
                {
                    int prevLength = sb.Length;
                    sb.Replace(lowerCase, "");
                    sb.Replace(upperCase, "");

                    if (sb.Length == prevLength)
                        break;
                }

                while (true)
                {
                    int prevLength = sb.Length;
                    foreach (var v in allPolys)
                        sb.Replace(v, "");

                    if (sb.Length == prevLength)
                        break;
                }

                if (sb.Length < min)
                    min = sb.Length;
            }

            Console.WriteLine("\n" + min);
        }

        private static void Part2MultiThreaded(List<string> allPolys, List<string> singlePolys, string fileContent)
        {
            Console.WriteLine("PART 2 MultiThreaded!");

            object lockObject = new object();
            int min = int.MaxValue;

            Parallel.ForEach(singlePolys, pair =>
            {
                Console.Write(pair);
                string lowerCase = pair;
                string upperCase = pair.ToUpper();

                StringBuilder sb = new StringBuilder(fileContent);

                while (true)
                {
                    int prevLength = sb.Length;
                    sb.Replace(lowerCase, "");
                    sb.Replace(upperCase, "");

                    if (sb.Length == prevLength)
                        break;
                }

                while (true)
                {
                    int prevLength = sb.Length;
                    foreach (var v in allPolys)
                        sb.Replace(v, "");

                    if (sb.Length == prevLength)
                        break;
                }

                lock (lockObject)
                {
                    if (sb.Length < min)
                        min = sb.Length;
                }
            });

            Console.WriteLine("\n" + min);
        }
    }
}
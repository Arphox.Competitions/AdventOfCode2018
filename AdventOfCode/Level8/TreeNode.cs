﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Level8
{
    public static partial class Level8Solver
    {
        private class TreeNode : IEnumerable<TreeNode>
        {
            internal int ChildCount { get; set; }
            internal int MetadataCount { get; set; }

            internal List<TreeNode> Children { get; } = new List<TreeNode>();
            internal List<int> Metadatas { get; set; }

            internal int GetValue()
            {
                if (ChildCount == 0)
                {
                    return Metadatas.Sum();
                }
                else
                {
                    int sum = 0;
                    foreach (int metadata in Metadatas)
                    {
                        int index = metadata - 1;
                        if (index >= 0 && index < Children.Count)
                        {
                            sum += Children[index].GetValue();
                        }
                    }

                    return sum;
                }
            }

            private IEnumerable<TreeNode> GetSelfAndAllChildrenRecursively()
            {
                yield return this;

                foreach (var child in Children)
                {
                    foreach (var c2 in child.GetSelfAndAllChildrenRecursively())
                    {
                        yield return c2;
                    }
                }
            }

            public IEnumerator<TreeNode> GetEnumerator()
            {
                foreach (var child in GetSelfAndAllChildrenRecursively())
                {
                    yield return child;
                }
            }

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }
    }
}
﻿namespace AdventOfCode.Level12
{
    public static partial class Level12
    {
        private static class Extender
        {
            internal static int Extend(ref string str, int currentZeroIndex)
            {
                const int PaddingLength = 5;

                int prefixDotCount = GetPrefixDotCount(str);
                int postfixDotCount = GetPostfixDotCount(str);

                if (prefixDotCount < PaddingLength)
                {
                    int newPrefixLength = PaddingLength - prefixDotCount;
                    string prefix = new string('.', newPrefixLength);
                    str = prefix + str;
                    currentZeroIndex += newPrefixLength;
                }

                if (postfixDotCount < PaddingLength)
                {
                    int newPostfixLength = PaddingLength - postfixDotCount;
                    string postfix = new string('.', newPostfixLength);
                    str += postfix;
                }

                return currentZeroIndex;
            }

            private static int GetPrefixDotCount(string str)
            {
                int i = 0;
                int counter = 0;
                while (str[i++] == '.')
                {
                    counter++;
                }

                return counter;
            }

            private static int GetPostfixDotCount(string str)
            {
                int i = str.Length - 1;
                int counter = 0;
                while (str[i--] == '.')
                {
                    counter++;
                }

                return counter;
            }
        }
    }
}
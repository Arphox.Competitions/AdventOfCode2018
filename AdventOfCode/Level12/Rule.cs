﻿using System;

namespace AdventOfCode.Level12
{
    public static partial class Level12
    {
        private sealed class Rule
        {
            internal string Pattern { get; }
            internal char CenterNewState { get; }

            internal Rule(string pattern, char centerNewState)
            {
                Pattern = pattern ?? throw new ArgumentNullException(nameof(pattern));
                CenterNewState = centerNewState;

                if (Pattern.Length != 5)
                    throw new ArgumentException($"{nameof(pattern)} has to be exactly 5 characters long.");
            }

            internal bool CanApply(string part)
            {
                return part == Pattern;
            }

            internal static Rule Parse(string LLCRRspaceN)
            {
                string pattern = LLCRRspaceN.Substring(0, 5);
                char newChar = LLCRRspaceN[6];
                return new Rule(pattern, newChar);
            }

            public override string ToString() => $"{Pattern} => {CenterNewState}";
        }
    }
}
﻿using System;
using System.IO;
using System.Linq;

namespace AdventOfCode
{
    public static class Level03
    {
        public static void Run()
        {
            const string inputPath = Program.PathBase + "level3.txt";

            int[,] fabric = new int[1001, 1001];
            string[] fileRows = File.ReadAllLines(inputPath);

            Part1(fabric, fileRows);
            Part2(fabric, fileRows);
        }

        private static void Part1(int[,] fabric, string[] fileRows)
        {
            foreach (string fileRow in fileRows)
            {
                //#1 @ 146,196: 19x14
                string[] rowParts = fileRow.Split(new char[] { ' ', '#', '@', ':' }, StringSplitOptions.RemoveEmptyEntries);
                int id = int.Parse(rowParts[0]);

                string[] marginSplitted = rowParts[1].Split(new char[] { ',', ':' }, StringSplitOptions.RemoveEmptyEntries);
                int marginLeft = int.Parse(marginSplitted[0]);
                int marginRight = int.Parse(marginSplitted[1]);

                string[] sizeSplitted = rowParts[2].Split(new char[] { 'x' }, StringSplitOptions.RemoveEmptyEntries);
                int rectWidth = int.Parse(sizeSplitted[0]);
                int rectHeight = int.Parse(sizeSplitted[1]);

                //Console.WriteLine($"{id} @ {marginLeft},{marginRight}: {rectHeight}x{rectWidth}");

                int iTo = marginLeft + rectWidth;
                int jTo = marginRight + rectHeight;
                for (int i = marginLeft; i < iTo; i++)
                {
                    for (int j = marginRight; j < jTo; j++)
                    {
                        fabric[i + 1, j + 1]++;
                    }
                }
            }

            Console.WriteLine(fabric.Cast<int>().Count(x => x > 1));
        }

        private static void Part2(int[,] fabric, string[] fileRows)
        {
            foreach (string fileRow in fileRows)
            {
                bool isIntact = true;

                //#1 @ 146,196: 19x14
                string[] rowParts = fileRow.Split(new char[] { ' ', '#', '@', ':' }, StringSplitOptions.RemoveEmptyEntries);
                int id = int.Parse(rowParts[0]);

                string[] marginSplitted = rowParts[1].Split(new char[] { ',', ':' }, StringSplitOptions.RemoveEmptyEntries);
                int marginLeft = int.Parse(marginSplitted[0]);
                int marginRight = int.Parse(marginSplitted[1]);

                string[] sizeSplitted = rowParts[2].Split(new char[] { 'x' }, StringSplitOptions.RemoveEmptyEntries);
                int rectWidth = int.Parse(sizeSplitted[0]);
                int rectHeight = int.Parse(sizeSplitted[1]);

                int iTo = marginLeft + rectWidth;
                int jTo = marginRight + rectHeight;
                for (int i = marginLeft; i < iTo; i++)
                {
                    for (int j = marginRight; j < jTo; j++)
                    {
                        if (fabric[i + 1, j + 1] != 1)
                        {
                            isIntact = false;
                        }
                    }
                }

                if (isIntact)
                {
                    Console.WriteLine(id);
                }
            }
        }
    }
}
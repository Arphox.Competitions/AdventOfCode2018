﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Level6
{
    public static partial class Level6Solver
    {
        /// <summary>
        ///     Hidden class inside its superclass to not pollute intellisense in upcoming levels
        /// </summary>
        private sealed class MapItem
        {
            private int Col { get; set; } // X
            private int Row { get; set; } // Y
            internal Level6IPoint RefPoint { get; set; }
            internal bool IsRefPoint => RefPoint != null;
            internal int SumOfDistances => PointDistances.Sum(x => x.Value);

            internal List<KeyValuePair<Level6IPoint, int>> PointDistances { get; set; } = new List<KeyValuePair<Level6IPoint, int>>();

            internal MapItem(int col, int row)
            {
                Col = col;
                Row = row;
            }

            internal void AddDistance(Level6IPoint point, int value)
            {
                PointDistances.Add(new KeyValuePair<Level6IPoint, int>(point, value));
            }

            internal bool IsAmbigous() // Works ONLY IF already ordered
            {
                return PointDistances[0].Value == PointDistances[1].Value;
            }

            public override string ToString()
            {
                string postfix = "";
                if (IsRefPoint)
                    postfix = " - REF!";

                return $"{Col}, {Row}{postfix}";
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode
{
    public static class Level02
    {
        public static void Run()
        {
            const string inputPath = Program.PathBase + "level2.txt";

            Part1(inputPath);
            Part2(inputPath);
        }

        private static void Part1(string inputPath)
        {
            char[] letters = new char['z' - 'a' + 1];
            for (int i = 0; i < letters.Length; i++)
            {
                letters[i] = (char)('a' + i);
            }

            int twos = 0;
            int threes = 0;

            var fileRows = File.ReadAllLines(inputPath);
            foreach (string fileRow in fileRows)
            {
                Dictionary<char, int> dict = new Dictionary<char, int>();
                foreach (char ch in fileRow)
                {
                    if (dict.ContainsKey(ch))
                    {
                        dict[ch]++;
                    }
                    else
                    {
                        dict[ch] = 1;
                    }
                }

                if (dict.Any(x => x.Value == 2))
                    twos++;
                if (dict.Any(x => x.Value == 3))
                    threes++;
            }

            Console.WriteLine(twos * threes);
        }

        private static void Part2(string inputPath)
        {
            string[] fileRows = File.ReadAllLines(inputPath);
            for (var i = 0; i < fileRows.Length - 1; i++)
            {
                for (var j = 1; j < fileRows.Length; j++)
                {
                    string a = fileRows[i];
                    string b = fileRows[j];

                    int dist = CalcLevenshteinDistance(a, b);
                    if (dist == 1)
                    {
                        Part2_PrintAnswer(a, b);
                        return;
                    }
                }
            }
        }

        private static void Part2_PrintAnswer(string a, string b)
        {
            string res = "";

            for (var i = 0; i < a.Length; i++)
            {
                if (a[i] == b[i])
                {
                    res += a[i];
                }
            }

            Console.WriteLine(res);
        }

        private static int CalcLevenshteinDistance(string a, string b)
        {
            if (String.IsNullOrEmpty(a) && String.IsNullOrEmpty(b))
            {
                return 0;
            }
            if (String.IsNullOrEmpty(a))
            {
                return b.Length;
            }
            if (String.IsNullOrEmpty(b))
            {
                return a.Length;
            }
            int lengthA = a.Length;
            int lengthB = b.Length;
            var distances = new int[lengthA + 1, lengthB + 1];
            for (int i = 0; i <= lengthA; distances[i, 0] = i++) ;
            for (int j = 0; j <= lengthB; distances[0, j] = j++) ;

            for (int i = 1; i <= lengthA; i++)
                for (int j = 1; j <= lengthB; j++)
                {
                    int cost = b[j - 1] == a[i - 1] ? 0 : 1;
                    distances[i, j] = Math.Min
                    (
                        Math.Min(distances[i - 1, j] + 1, distances[i, j - 1] + 1),
                        distances[i - 1, j - 1] + cost
                    );
                }
            return distances[lengthA, lengthB];
        }
    }
}